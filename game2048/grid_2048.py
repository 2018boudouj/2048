import random as rd
import copy
from game2048.textual_2048 import *

THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "game2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}


def create_grid(x=4):       ## Creer la grille de jeu
    game_grid = []
    for i in range(0,x):
        game_grid.append(x*[0])
    return game_grid

def grid_add_new_tile(grille):      ## permet d'ajouter un 2 ou un 4 aleatoirement a une postion ou il y avait un 0
    x,y=get_new_position(grille)
    grille[x][y]=get_value_new_tile()
    return grille

def get_value_new_tile():       ## permet de choisr entre un 2 et un 4 avec 90% de chance pour un 2 et 10% pour un 4
    new_value=rd.choices([2,4],weights=[0.9,0.1],k=1)
    return new_value[0]

def get_all_tiles(grille):      ## permet de recuperer toutes les valeurs de la grille
    l=[]
    for elt in grille:
        for k in elt:
            if k==' ':
                l.append(0)
            else:
                l.append(k)
    return(l)

def get_empty_tiles_positions(grille):      ##permet d'avoir les positions ou il y a des 0 dans la grille
    l=len(grille)
    Position=[]
    for i in range(0,l):
        for j in range (0,l):
            if grille[i][j] == 0 or grille[i][j]==' ':
                Position.append((i,j))
    return(Position)

def get_new_position(grille):       ## decide de la future position
    L=get_empty_tiles_positions(grille)
    pos=rd.choice(L)
    return pos

def grid_get_value(grille,x,y):     ##avoir la valeur d'une case de la grille
    if grille[x][y]:
        return 0
    else:
        return grille[x][y]

def init_game(x=4):         ## creer le plateau de jeu initial
    grid=create_grid(x)
    grid=grid_add_new_tile(grid)
    grid=grid_add_new_tile(grid)
    return grid

def grid_to_string(grille, x):

    grid="""
"""
    for i in range(x):
        grid+= x*""" ==="""
        grid+="""
"""
        for j in range(x):
            a= grille[i][j]
            grid+="""| """+str(a)+""" """
        grid+="""|"""
        grid+="""
"""
    grid+= x*""" ==="""
    grid+="""
    """
    return(grid)

def long_value(grid):
    long_max=0
    for elt in grid:
        for k in elt:
            if len(str(k))>long_max:
                long_max=len(str(k))
    return(long_max)

def grid_to_string_with_size(grid,n):
    max=long_value(grid)
    grille="""
"""
    for i in range(n):
        grille=grille+n*(""" ="""+(max*"""=""")+"""=""")+"""
"""
        for j in range(n):
            a=grid[i][j]
            grille=grille+"""| """+str(a)+((max-len(str(a))+1)*""" """)
        grille+="""|"""
        grille+="""
"""
    grille=grille+n*(""" ="""+(max*"""=""")+"""=""")+"""
    """
    return(grille)

def long_value_with_theme(grille,theme):
    long_max=0
    for elt in grille:
        for k in elt:
            if k>long_max:
                long_max=k
    val=theme[long_max]
    taille=len(val)
    return(taille)

def grid_to_string_with_size_and_theme(grid,theme,n):
    max=long_value_with_theme(grid,theme)
    grille="""
"""
    for i in range(n):
        grille=grille+n*(""" ="""+(max*"""=""")+"""=""")+"""
"""
        for j in range(n):
            a=grid[i][j]
            val=theme[a]
            grille=grille+"""| """+str(val)+((max-len(str(val))+1)*""" """)
        grille+="""|"""
        grille+="""
"""
    grille=grille+n*(""" ="""+(max*"""=""")+"""=""")+"""
"""
    return(grille)

def move_row_left(row):
    l=len(row)
    k=0
    for i in range (l-1):
        while row[i]==0 and k<l:
            k=k+1
            for j in range(i+1,l):
                    row[j],row[j-1]=row[j-1],row[j]
    for i in range (l-1):
        if row[i]==row[i+1]:
            row[i]=2*row[i]
            row[i+1]=0
    k=0
    for i in range (l-1):
        while row[i]==0 and k<l:
            k=k+1
            for j in range(i+1,l):
                    row[j],row[j-1]=row[j-1],row[j]
    return row

def move_row_right(row):
    l=len(row)
    L_intermediare=row[-1:-l-1:-1]
    Res_intermediare=move_row_left(L_intermediare)
    Res=Res_intermediare[-1:-l-1:-1]
    return Res

def move_grid(grid,d):
    l=len(grid)
    if d == "left":
        for i in range(l) :
            new=move_row_left(grid[i])
            grid[i]=new
    if d == "right":
        for i in range(l):
            new=move_row_right(grid[i])
            grid[i]=new
    if d == "up":
        for i in range(l):
            L=[]
            for j in range (l):
                L.append(grid[j][i])
            new=move_row_left(L)
            for j in range (l):
                grid[j][i]=new[j]
    if d == "down":
        for i in range(l):
            L=[]
            for j in range (l):
                L.append(grid[j][i])
            new=move_row_right(L)
            for j in range (l):
                grid[j][i]=new[j]
    return grid

def is_grid_full(grid):
    L=[]
    for elt in grid:
        for k in elt :
            if k==0:
                L.append(k)
    if len(L)==0:
        return True
    else:
        return False

def move_possible(grid):
    L=["rigth","left","up","down"]
    Res=[]
    for d in L:
        grille=copy.deepcopy(grid)
        grille=move_grid(grille,d)
        if not is_grid_full(grille) :
            Res.append(True)
        else:
            Res.append(False)
    return Res

def is_game_over(grid):
    L=move_possible(grid)
    if not (True in L):
        return True
    else:
        return False

def get_grid_tile_max(grid):
    max=0
    for elt in grid:
        for k in elt:
            if k>max:
                max=k
    return max

def random_play():
    L={0:"right",1:"left",2:"up",3:"down"}
    grid=init_game()
    while not is_game_over(grid):
        Move=move_possible(grid)
        next=[]
        for k in range(4):
            if Move[k]:
                next.append(L[k])
        d=rd.choice(next)
        grid=move_grid(grid,d)
        grid=grid_add_new_tile(grid)
    if int(get_grid_tile_max(grid))>=2048:
        return ("You won !")
    else:
        return("You lose !")

def ask_and_read_size():
    return read_size_grid()

def ask_and_read_theme():
    return read_theme_grid()


def game_play():
    size=ask_and_read_size()
    th=ask_and_read_theme()
    theme=THEMES[th]
    grid=init_game()
    grille=grid_to_string_with_size_and_theme(grid,theme,size)
    print(grille)
    while not is_game_over(grid):
        move=read_player_command()
        grid=move_grid(grid,move)
        grid=grid_add_new_tile(grid)
        grille=grid_to_string_with_size_and_theme(grid,theme,size)
        print(grille)
    if int(get_grid_tile_max(grid))>=2048:
        return ("You won !")
    else:
        return("You lose !")









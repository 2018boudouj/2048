from tkinter import *
from game2048.grid_2048 import *

THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "game2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}


root = Tk() # Création de la fenêtre racine
root.title('2048')
root.resizable(False, False)
root.geometry("600x600-500+0")
root2=Toplevel()
root2.title('2048')
root2.grid()


def play():
    th=theme_choice()
    grid_size_2048=int(size_choice())
    grid = init_game(grid_size_2048)
    score=0

    dic={"Default":"0","Chemistry":"1","Alphabet":"2"}

    theme=THEMES[dic[th]]

    TILES_BG_COLOR = {0: "#9e948a", 2: "#eee4da", 4: "#ede0c8", 8: "#f1b078", \
                      16: "#eb8c52", 32: "#f67c5f", 64: "#f65e3b", \
                      128: "#edcf72", 256: "#edcc61", 512: "#edc850", \
                      1024: "#edc53f", 2048: "#edc22e", 4096: "#5eda92", \
                      8192: "#24ba63"}

    TILES_FG_COLOR = {0: "#776e65", 2: "#776e65", 4: "#776e65", 8: "#f9f6f2", \
                      16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2", \
                      256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2", \
                      2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}


    def graphical_grid_init(game_grid) :
        frames=[[] for k in range(grid_size_2048)]
        labels=[[] for k in range(grid_size_2048)]

        for i in range(grid_size_2048) :
            for j in range(grid_size_2048) :

                number=game_grid[i][j]
                bg_color,fg_color=TILES_BG_COLOR[number],TILES_FG_COLOR[number]

                frames[i].append(Frame(root2,bd=1,relief='solid',width=600/grid_size_2048, height=600/grid_size_2048,bg=bg_color))
                frames[i][j].grid(row=i, column=j)
                frames[i][j].grid_propagate(0)
                labels[i].append(Label(frames[i][j], text = theme[number],padx=0 , pady= 0,bg=bg_color,fg=fg_color,font=("Verdana", 40, "bold"), anchor='se'))
                labels[i][j].grid()
        return frames,labels

    def graphical_grid_update(grid,move) :
        #transforme la grid selon un mouvement move dans ['left','right','up,'down']

        #retourne une fonction qui prend un argument un evenement
        #et qui effectue l'affichage graphique correspondant au movement

        def updater(event) :
            grid_copy=copy.deepcopy(grid)
            move_grid(grid,move)
            if not is_grid_full(grid) and grid_copy != grid :
                grid_add_new_tile(grid)
            for i in range(grid_size_2048) :
                for j in range(grid_size_2048) :
                    number=grid[i][j]
                    bg_color,fg_color=TILES_BG_COLOR[number],TILES_FG_COLOR[number]

                    frames[i][j].config(bg=bg_color)
                    labels[i][j].config(text=theme[number],bg=bg_color,fg=fg_color)
        return updater


    frames,labels=graphical_grid_init(grid)

    root2.bind('<KeyPress-Down>',graphical_grid_update(grid,'down'))
    root2.bind('<KeyPress-Up>',graphical_grid_update(grid,'up'))
    root2.bind('<KeyPress-Right>',graphical_grid_update(grid,'right'))
    root2.bind('<KeyPress-Left>',graphical_grid_update(grid,'left'))

def theme_choice():
    th=text2.get()
    return th
def size_choice():
    size=text.get()
    return size

f1=Frame(root)
f1.grid(row=0,column=0)
Label(f1,text='Choissisez une taille').grid(row=0,column=0)
text=StringVar(f1)
taille=Entry(f1, textvariable=text)
taille.grid(row=1,column=0)


f2=Frame(root)
f2.grid(row=1, column=0)
Label(f2, text="""Choissez un thème parmis les suivants :
    - Default
    - Chemistry
    - Alphabet
""").grid(row=0, column=0)
text2=StringVar(f2)
theme=Entry(f2, textvariable=text2)
theme.grid(row=1, column=0)

f3=Frame(root)
f3.grid(row=3, column=0)
but=Button(f3, text='play',command=play)
but.grid(row=0,column=0)
but2=Button(f3, text='quit', command=quit)
but2.grid(row=0, column=1)

root.mainloop()

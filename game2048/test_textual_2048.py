from game2048.textual_2048 import read_player_command
import random as rd


def test_mock_input_return(monkeypatch):
    def mock_return(arg):
        return rd.choice(["g","d","h","b"])
    monkeypatch.setattr('bultins.input',mock_return())
    move=read_player_command()
    assert move in ['h','b','g','d']
    assert move !="t"

